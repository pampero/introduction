---
title: "Discurso de Pedro Puerta"
image: "profile.png"
weight: 8
---

Hoy nos están viendo desde toda la provincia en esta nueva modalidad que tenemos, creo que es el primer lanzamiento de este modo. Quiero agradecerles a todos los que están del otro lado, y muy especialmente a los intendentes de las distintas localidades que componen nuestro espacio político, muchas gracias por acompañarnos..
